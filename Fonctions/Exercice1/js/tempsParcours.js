function calculerTempsParcoursSec(prmVitesse,prmDistance) {
    let returnvalue = prmDistance/prmVitesse*3600;
    return returnvalue;
}
function convertiseur(prmtempensec) {
    let heures;
    let minutes;
    let secondes;
    let returnvalue;
    heures = Math.floor(prmtempensec/3600);
    minutes = Math.floor((prmtempensec/60) - (heures*60));
    secondes = Math.floor(prmtempensec- (minutes*60) - (heures*3600))
    returnvalue = heures + "h " + minutes + "min " + secondes + "s";
    return returnvalue;
}
let vitesse = 90;
let distance = 500; 
console.log("Calcul du temps de parcours d'un trajet :");
console.log("Vitesse moyenne (en km/h) :   "+vitesse);
console.log("Distance à parcourir (en km): "+distance);
console.log("A " + vitesse + " km/h, une distance de " + distance + " km est parcourue en " + calculerTempsParcoursSec(vitesse,distance) + " s");
tempensec = calculerTempsParcoursSec(vitesse,distance);
console.log("A " + vitesse + " km/h, une distance de " + distance + " km est parcourue en " + convertiseur(tempensec));
