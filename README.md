# <b> TP Javascript </b>

### <b> Mes TP en JavaScript</b>

> * Auteur : Vincent AGENEAU
> * Date de publication : 11/05/2021
## <b>Sommaire</b>

  - [HelloWorld](Introduction/HelloWorld_js/index.html)
  - <b>Variables et Operateurs </b>
    -  [Activité](Variables_et_Operateurs/Calcul_Surface/index.html)
    -  [Exercice 1](Variables_et_Operateurs/Exercice1/index.html)
    -  [Exercice 2](Variables_et_Operateurs/Exercice2/index.html)
  - <b>Structures de Contrôle </b>
    -  [Activité](Structures_de_Contrôle/Suite_Syracuse/index.html)
    -  [Exercice 1](Structures_de_Contrôle/Exercice1/index.html)
    -  [Exercice 2](Structures_de_Contrôle/Exercice2/index.html)
    -  [Exercice 3](Structures_de_Contrôle/Exercice3/index.html)
    -  [Exercice 4](Structures_de_Contrôle/Exercice4/index.html)
    -  [Exercice 5](Structures_de_Contrôle/Exercice5/index.html)
  - <b>Fonctions</b>
    -  [Activité](Fonctions/Calcul_IMC/index.html)
    -  [Exercice 1](Fonctions/Exercice1/index.html)
    -  [Exercice 2](Fonctions/Exercice2/index.html)
  - <b>Les objets</b> 
    -  [Activité 1](Objets/Activite/index.html)
    -  [Activité 2](Objets/Activite2/index.html)
    -  [Activité 3](Objets/Activite3/index.html)
    -  [Héritage Prototype](Objets/Heritage_Proto/index.html)
    -  [Exercice](Objets/Exercice/index.html)
  - <b>Les tableaux</b> 
    -  [Activité 1](Tableaux/Calcul_Moyenne/index.html)
    -  [Activité 2](Tableaux/Liste_Articles/index.html)
    -  [Activité 3](Tableaux/Tableau_Patient_IMC/index.html)
    -  [Exercice 1](Tableaux/Histogramme/index.html)