function Personnage(prmNom, prmNiveau) {
    this.nom = prmNom;
    this.niveau = prmNiveau;

}

Personnage.prototype.saluer = function () {
    let message;
    message = this.nom + " vous salue !!";
    return message;
}

function Guerrier(prmNom, prmNiveau, prmArmes) {
    Personnage.call(this, prmNom, prmNiveau);
    this.armes = prmArmes;
}

Guerrier.prototype = Object.create(Personnage.prototype);

function Magicien(prmNom, prmNiveau, prmPouvoir) {
    Personnage.call(this, prmNom, prmNiveau);
    this.pouvoir = prmPouvoir;
}

Magicien.prototype = Object.create(Personnage.prototype);

Guerrier.prototype.combattre = function () {
    let message;
    message = this.nom + " est un guerrier qui se bat avec une " + this.armes;
    return message;
}

Magicien.prototype.posseder = function () {
    let message;
    message = this.nom + " est un magicien qui possède le pouvoir de " + this.pouvoir;
    return message;
}

let objPerso1 = new Guerrier('Arthur', 3, 'épée');
let objPerso2 = new Magicien('Merlin', 2, 'prédire les batailles');
console.log(objPerso1.saluer());
console.log(objPerso1.combattre());
console.log(objPerso2.saluer());
console.log(objPerso2.posseder());

