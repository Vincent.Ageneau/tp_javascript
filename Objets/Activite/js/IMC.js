let objPatient = {
    nom: 'Dupond',
    prenom: 'Jean',
    age: 30,
    sexe: 'masculin',
    pronom: 'il',
    taille: 180, //cm
    poids: 85, //kg
    decrire: function () {
        let description = "";
        description = "le patient " + this.prenom + " " + this.nom + " de sexe " + this.sexe + " est agé de " + this.age + " ans. Il mesure " + this.taille + " cm et pèse " + this.poids + " kg";
        return description;
    },
    definir_corpulence: function () {


          function CalculerImc() {
            let calculimc = 0;
            calculimc = objPatient.poids / ((objPatient.taille / 100) * (objPatient.taille / 100));
            return calculimc;
        }
        let imc = CalculerImc().toFixed(2);
        function interpreter_IMC (prmIMC) {
            let interpretation = "";
        if (prmIMC < 16.5) {
            interpretation =objPatient.pronom+" est en situation "+ "de dénutrition";
        } else if ((prmIMC >= 16.5) && (prmIMC < 18.5)) {
            interpretation =objPatient.pronom+" est en situation "+ "de maigreur";
        } else if ((prmIMC >= 18.5) && (prmIMC < 25)) {
            interpretation =objPatient.pronom+" est en situation "+ "de corpulence normale";
        } else if ((prmIMC >= 25) && (prmIMC < 30)) {
            interpretation =objPatient.pronom+" est en situation "+ "de surpoids";
        } else if ((prmIMC >= 30) && (prmIMC < 35)) {
            interpretation =objPatient.pronom+" est en situation "+ "d'obésité modérée";
        } else if ((prmIMC >= 35) && (prmIMC <= 40)) {
            interpretation =objPatient.pronom+" est en situation "+ "d'obésité sévère";
        } else if (prmIMC > 40) {
            interpretation = objPatient.pronom+" est en situation "+ "d'obésité morbide";
        }
        return interpretation;
            
        }
        let interpret = interpreter_IMC(imc);
        let affichage ="Son IMC est de: "+imc+" \n"+interpret;
        return affichage;
    }
};

console.log(objPatient.decrire());
console.log(objPatient.definir_corpulence());
