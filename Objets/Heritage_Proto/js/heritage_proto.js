function Personne(prmNom, prmPrenom, prmAge, prmSexe) {
    this.nom = prmNom;
    this.prenom = prmPrenom;
    this.age = prmAge;
    this.sexe = prmSexe;
}

Personne.prototype.decrire = function() {
    let description;
    description = "Cette personne s'appelle " + this.prenom + " " + this.nom + " elle est agée de " + this.age + " ans" ;
    return description;
}

function Professeur(prmNom, prmPrenom, prmAge, prmSexe, prmMatiere) {
    Personne.call(this,prmNom, prmPrenom, prmAge, prmSexe);
    this.matiere = prmMatiere;
}

Professeur.prototype = Object.create(Personne.prototype);

function eleve(prmNom, prmPrenom, prmAge, prmSexe,prmclasse) {
    Personne.call(this,prmNom, prmPrenom, prmAge, prmSexe);
    this.classe = prmclasse;
    
}

eleve.prototype = Object.create(Personne.prototype);

Professeur.prototype.decrire_plus = function() {
    let description;
    let prefixe;
    if((this.sexe == 'masculin')||(this.sexe == 'Masculin')) {
        prefixe = 'Mr';
    } else if((this.sexe == 'feminin')||(this.sexe == 'Feminin')){
        prefixe = 'Mme';
    }
    description = prefixe + " " + this.prenom + " " + this.nom + " est professeur de " + this.matiere;
    return description;
}

eleve.prototype.decrire_plus = function() {
    let description;
    let prefixe;
    if((this.sexe == 'masculin')||(this.sexe == 'Masculin')) {
        prefixe = 'Un eleve';
    } else if((this.sexe == 'feminin')||(this.sexe == 'Feminin')){
        prefixe = 'Une eleve';
    }
    description =this.prenom + " " + this.nom + " est "+prefixe +" de "+ this.classe;
    return description;
}

let objProfesseur1 = new Professeur('Dupond', 'Jean', 30, 'masculin', 'Mathématiques');
console.log(objProfesseur1.decrire());
console.log(objProfesseur1.decrire_plus());
let objeleve1 = new eleve('Dutillieul','Dorian',18,'Masculin','SNIR1');
console.log(objeleve1.decrire());
console.log(objeleve1.decrire_plus());
