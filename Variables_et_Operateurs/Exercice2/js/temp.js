let Celcius = 22.6;
let Fahrenheit = Celcius * 1.8 + 32;
console.log("Conversion Celcius (°C) / Fahrenheit (°F)");
console.log("Une température de " + Celcius + "°C correspond à une température de " + Fahrenheit.toFixed(1) + "°F");
