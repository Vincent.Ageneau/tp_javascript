let cm = 160;
let kilo = 100;
cm /= 100;
cm *= cm;
let div = kilo / cm;
console.log("Calcul de l'IMC :");
console.log("Taille : " + cm.toFixed(1));
console.log("Poids : " + kilo.toFixed(1));
console.log("IMC : " + div.toFixed(1));