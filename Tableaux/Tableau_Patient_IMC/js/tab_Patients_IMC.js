function patient(prmNom, prmPrenom, prmAge, prmSexe, prmTaille, prmPoids) {
    this.nom = prmNom;
    this.prenom = prmPrenom;
    this.age = prmAge;
    this.sexe = prmSexe;
    this.taille = prmTaille; //cm
    this.poids = prmPoids; //kg
    this.IMC = this.poids / ((this.taille / 100) * (this.taille / 100));
}

patient.prototype.decrire = function () {
    let genre = this.sexe;
    let description = "";
    if ((genre == 'Masculin') || (genre == 'masculin')) {
        description = "le patient " + this.prenom + " " + this.nom + " de sexe " + this.sexe + " est agé de " + this.age + " ans. il mesure " + this.taille + " cm et pèse " + this.poids + " kg";
    } else if ((genre == 'Feminin') || (genre == 'feminin')) {
        description = "la patiente " + this.prenom + " " + this.nom + " de sexe " + this.sexe + " est agée de " + this.age + " ans. elle mesure " + this.taille + " cm et pèse " + this.poids + " kg";
    }

    return description;
}

patient.prototype.definir_corpulence = function (prmObj) {


    function CalculerImc() {
        let calculimc = 0;
        calculimc = prmObj.poids / ((prmObj.taille / 100) * (prmObj.taille / 100));
        return calculimc;
    }
    let imc = CalculerImc().toFixed(2);
    let genre = prmObj.sexe;
    function interpreter_IMC(prmIMC) {
        let interpretation = "";
        if ((genre == 'Masculin') || (genre == 'masculin')) {
            if (prmIMC < 16.5) {
                interpretation = "Il est en situation " + "de denutrition";
            } else if ((prmIMC >= 16.5) && (prmIMC < 18.5)) {
                interpretation = "Il est en situation " + "de maigreur";
            } else if ((prmIMC >= 18.5) && (prmIMC < 25)) {
                interpretation = "Il est en situation " + "de corpulence normal";
            } else if ((prmIMC >= 25) && (prmIMC < 30)) {
                interpretation = "Il est en situation " + "de surpoids";
            } else if ((prmIMC >= 30) && (prmIMC < 35)) {
                interpretation = "Il est en situation " + "d' obésité modéré";
            } else if ((prmIMC >= 35) && (prmIMC <= 40)) {
                interpretation = "Il est en situation " + "d' obésité severe";
            } else if (prmIMC > 40) {
                interpretation = "Il est en situation " + "d'Obésité morbide";
            }
        } else if ((genre == 'Feminin') || (genre == 'feminin')) {
            if (prmIMC < 14.5) {
                interpretation = "Elle est en situation " + "de denutrition";
            } else if ((prmIMC >= 14.5) && (prmIMC < 16.5)) {
                interpretation = "Elle est en situation " + "de maigreur";
            } else if ((prmIMC >= 16.5) && (prmIMC < 23)) {
                interpretation = "Elle est en situation " + "de corpulence normal";
            } else if ((prmIMC >= 23) && (prmIMC < 28)) {
                interpretation = "Elle est en situation " + "de surpoids";
            } else if ((prmIMC >= 28) && (prmIMC < 33)) {
                interpretation = "Elle est en situation " + "d' obésité modéré";
            } else if ((prmIMC >= 33) && (prmIMC <= 38)) {
                interpretation = "Elle est en situation " + "d' obésité severe";
            } else if (prmIMC > 38) {
                interpretation = "Elle est en situation " + "d'Obésité morbide";
            }
        }
        return interpretation;

    }
    let interpret = interpreter_IMC(imc);
    let affichage = "Son IMC est de: " + imc + " \n" + interpret;
    return affichage;
}

let objPatient1 = new patient('Dupond', 'Jean', 30, 'Masculin', 180, 85);
let objPatient3 = new patient('Martin', 'Eric', 42, 'Masculin', 165, 90);
let objPatient2 = new patient('Moulin', 'Isabelle', 46, 'Feminin', 158, 74);
let objPatient4 = new patient('Verwaerde', 'Paul', 55, 'Masculin', 177, 66);
let objPatient5 = new patient('Durand', 'Dominique', 60, 'Feminin', 163, 54);
let objPatient6 = new patient('Lejeune', 'Bernard', 63, 'Masculin', 158, 78);
let objPatient7 = new patient('Chevalier', 'Louise', 35, 'Feminin', 170, 82);

let tabPatient = [objPatient1, objPatient2, objPatient3, objPatient4, objPatient5, objPatient6, objPatient7];
let nm = "";
let prmn = "";
let Imd = 0;
let aff = "";
afficher_ListePatients = function (prmTabPatient) {
    for (let i = 0; i < prmTabPatient.length; i++) {
        nm = prmTabPatient[i].nom;
        prmn = prmTabPatient[i].prenom;
        aff = prmn + " " + nm;
        console.log(aff);
    }
};

console.log("\nListes des patients\n\n")
let af = afficher_ListePatients(tabPatient);

afficher_ListePatients_par_sexe = function (prmTabPatient, prmSexe) {
    for (let i = 0; i < prmTabPatient.length; i++) {
        if (prmSexe == 'Feminin') {
            if (prmTabPatient[i].sexe == 'Feminin') {
                nm = prmTabPatient[i].nom;
                prmn = prmTabPatient[i].prenom;
                aff = prmn + " " + nm;
                console.log(aff);
            }

        } else if (prmSexe == 'Masculin') {

            if (prmTabPatient[i].sexe == 'Masculin') {
                nm = prmTabPatient[i].nom;
                prmn = prmTabPatient[i].prenom;
                aff = prmn + " " + nm;
                console.log(aff);
            }
        }
    }
};
console.log("\nListes des patients Masculins\n\n")
afficher_ListePatients_par_sexe(tabPatient, 'Masculin')
console.log("\nListes des patients Feminin\n\n")
afficher_ListePatients_par_sexe(tabPatient, 'Feminin')

afficher_ListePatients_par_corpulence = function (prmTabPatient, prmCorpulence) {
    console.log("\nListe des patient en état de " + prmCorpulence + " \n\n");
    for (let i = 0; i < prmTabPatient.length; i++) {

        let corpulence = "";
        let genr = prmTabPatient[i].sexe;
        if ((genr == 'Masculin') || (genr == 'masculin')) {
            if (prmTabPatient[i].IMC < 16.5) {
                corpulence = "denutrition";
            } else if ((prmTabPatient[i].IMC >= 16.5) && (prmTabPatient[i].IMC < 18.5)) {
                corpulence = "maigreur";
            } else if ((prmTabPatient[i].IMC >= 18.5) && (prmTabPatient[i].IMC < 25)) {
                corpulence = "normal";
            } else if ((prmTabPatient[i].IMC >= 25) && (prmTabPatient[i].IMC < 30)) {
                corpulence = "surpoids";
            } else if ((prmTabPatient[i].IMC >= 30) && (prmTabPatient[i].IMC < 35)) {
                corpulence = "obésité modéré";
            } else if ((prmTabPatient[i].IMC >= 35) && (prmTabPatient[i].IMC <= 40)) {
                corpulence = "obésité severe";
            } else if (prprmTabPatient[i].IMCmIMC > 40) {
                corpulence = "obésité morbide";
            }
        } else if ((genr == 'Feminin') || (genr == 'feminin')) {
            if (prmTabPatient[i].IMC < 14.5) {
                corpulence = "denutrition";
            } else if ((prmTabPatient[i].IMC >= 14.5) && (prmTabPatient[i].IMC < 16.5)) {
                corpulence = "maigreur";
            } else if ((prmTabPatient[i].IMC >= 16.5) && (prmTabPatient[i].IMC < 23)) {
                corpulence = "normal";
            } else if ((prmTabPatient[i].IMC >= 23) && (prmTabPatient[i].IMC < 28)) {
                corpulence = "surpoids";
            } else if ((prmTabPatient[i].IMC >= 28) && (prmTabPatient[i].IMC < 33)) {
                corpulence = "obésité modéré";
            } else if ((prmTabPatient[i].IMC >= 33) && (prmTabPatient[i].IMC <= 38)) {
                corpulence = "obésité severe";
            } else if (prmTabPatient[i].IMC > 38) {
                corpulence = "obésité morbide";
            }
        }
        if (corpulence == prmCorpulence) {
            nm = prmTabPatient[i].nom;
            prmn = prmTabPatient[i].prenom;
            Imd = prmTabPatient[i].IMC
            aff = prmn + " " + nm + " avec un IMC = " + Imd.toFixed(2);
            console.log(aff);
        }
    }

}

afficher_ListePatients_par_corpulence(tabPatient, 'obésité modéré');

let affro = "Ce nom n'existe pas dans la liste";
let corp = "";
afficher_DescriptionPatient = function (prmTabPatient, prmNom) {
    console.log("\nDescription du patien " + prmNom + "\n\n");
    for (let i = 0; i < prmTabPatient.length; i++) {
        if (prmTabPatient[i].nom == prmNom) {
            let gendr = prmTabPatient[i].sexe;
            if ((gendr == 'Masculin') || (genr == 'masculin')) {
                if (prmTabPatient[i].IMC < 16.5) {
                    corp = "denutrition";
                } else if ((prmTabPatient[i].IMC >= 16.5) && (prmTabPatient[i].IMC < 18.5)) {
                    corp = "maigreur";
                } else if ((prmTabPatient[i].IMC >= 18.5) && (prmTabPatient[i].IMC < 25)) {
                    corp = "normal";
                } else if ((prmTabPatient[i].IMC >= 25) && (prmTabPatient[i].IMC < 30)) {
                    corp = "surpoids";
                } else if ((prmTabPatient[i].IMC >= 30) && (prmTabPatient[i].IMC < 35)) {
                    corp = "obésité modéré";
                } else if ((prmTabPatient[i].IMC >= 35) && (prmTabPatient[i].IMC <= 40)) {
                    corp = "obésité severe";
                } else if (prmTabPatient[i].IMC > 40) {
                    corp = "obésité morbide";
                }
            } else if ((gendr == 'Feminin') || (genr == 'feminin')) {
                if (prmTabPatient[i].IMC < 14.5) {
                    corp = "denutrition";
                } else if ((prmTabPatient[i].IMC >= 14.5) && (prmTabPatient[i].IMC < 16.5)) {
                    corp = "maigreur";
                } else if ((prmTabPatient[i].IMC >= 16.5) && (prmTabPatient[i].IMC < 23)) {
                    corp = "normal";
                } else if ((prmTabPatient[i].IMC >= 23) && (prmTabPatient[i].IMC < 28)) {
                    corp = "surpoids";
                } else if ((prmTabPatient[i].IMC >= 28) && (prmTabPatient[i].IMC < 33)) {
                    corp = "obésité modéré";
                } else if ((prmTabPatient[i].IMC >= 33) && (prmTabPatient[i].IMC <= 38)) {
                    corp = "obésité severe";
                } else if (prmTabPatient[i].IMC > 38) {
                    corp = "obésité morbide";
                }
            }
            let amd = tabPatient[i].IMC;
            affro = "Le patient " + prmTabPatient[i].prenom + " " + prmTabPatient[i].nom + " est agé de " + prmTabPatient[i].age + " ans. Il mesure " + prmTabPatient[i].taille + " cm et pèse " + prmTabPatient[i].poids + " kg\nL'IMC de ce patient est de : " + amd.toFixed(2) + " \nIl est en situation de " + corp;

        } else {
            console.log(affro);

        }


    }

}

afficher_DescriptionPatient(tabPatient, 'Edouard');
afficher_DescriptionPatient(tabPatient, 'Dupond');